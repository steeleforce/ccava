@isTest
private class cc_hello_ctrl_pmt_MCC_EditTest {

	@isTest static void testSaveMCC() {
		ccrz__E_StoredPayment__c pmt = new ccrz__E_StoredPayment__c(
			ccrz__AccountNumber__c = '1111111',
			ccrz__AccountType__c = 'mcc',
			ccrz__PaymentType__c = 'Visa',
			ccrz__DisplayName__c =  'aaaaaaa',
			ccrz__Enabled__c = true,
			ccrz__Storefront__c = 'teststore');
		insert pmt;

		Map<String,Object> mccData = new Map<String,Object>{
			'storedPaymentId' => pmt.Id,
			'accountNumber' => '2222222',
			'accountName' => 'bbbbbbbb',
			'isEnabled' => false
		};
		String jsonMCCData = JSON.serialize(mccData);

		Test.startTest();

		PageReference pr = new PageReference('someurl');
		pr.getParameters().put('pid', pmt.Id);
		Test.setCurrentPage(pr);

		cc_hello_ctrl_pmt_MCC_Edit controller = new cc_hello_ctrl_pmt_MCC_Edit();
		System.assertEquals('aaaaaaa', controller.displayName);

		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ccrz.cc_RemoteActionResult result = cc_hello_ctrl_pmt_MCC_Edit.updateCCNumber(ctx, jsonMCCData);
		System.assert(!result.success);

		// test with bad payment id
		// mccData.put('storedPaymentId', User.);
		// jsonMCCData = JSON.serialize(mccData);
		// result = cc_hello_ctrl_pmt_MCC_Edit.updatePONumber(ctx, jsonMCCData);
		// System.assert(!result.success);

		Test.stopTest();
	}

	@isTest static void testFailSaveMCC() {
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ccrz.cc_RemoteActionResult result = cc_hello_ctrl_pmt_MCC_Edit.updateCCNumber(ctx, 'notvalid');
		System.assert(!result.success);
	}
}
