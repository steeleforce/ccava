@isTest
private class cc_hello_UninstallHandlerTest {

	@isTest static void testUninstallHandlerWithNoPage() {
		Test.testUninstall(new cc_hello_UninstallHandler());
		// making it here without an exception is a pass
	}

	@isTest static void testUninstallHandlerWithPage() {
		insert new ccrz__E_Subscriber_Page__c(Name='ccrzhello', ccrz__PageKey__c='ccrzhello');

		Test.testUninstall(new cc_hello_UninstallHandler());

		List<ccrz__E_Subscriber_Page__c> subPages = [select Id from ccrz__E_Subscriber_Page__c where ccrz__PageKey__c = 'ccrzhello'];
		System.assertEquals(0, subPages.size(), 'Uninstall did not remove the ccrzhello subscriber page record');
	}
}