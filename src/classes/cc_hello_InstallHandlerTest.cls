@isTest
private class cc_hello_InstallHandlerTest {

	@isTest static void testInstallHandler() {
		cc_hello_InstallHandler postinstall = new cc_hello_InstallHandler();

		Test.testInstall(postinstall, null);
		List<ccrz__E_Subscriber_Page__c> subPages = [select Id from ccrz__E_Subscriber_Page__c where ccrz__PageKey__c = 'ccrzhello'];
		System.assertEquals(1, subPages.size(), 'Subscriber page was not created on new install');

		Test.testInstall(postinstall, new Version(1,0), false);
		subPages = [select Id from ccrz__E_Subscriber_Page__c where ccrz__PageKey__c = 'ccrzhello'];
		System.assertEquals(1, subPages.size(), 'Should only be one subscriber page after upgrade');
	}
}