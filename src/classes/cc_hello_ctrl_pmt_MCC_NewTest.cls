@IsTest
private class cc_hello_ctrl_pmt_MCC_NewTest {

	@isTest
	static void testSaveMCC() {
        ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
        ctx.storefront = 'teststore';

        Map<String,Object> mccData = new Map<String,Object>{
        	'accountNumber' => '1111111',
        	'accountType' => 'mcc',
			'paymentType' => 'Visa',
        	'accountName' => 'aaaaaaa',
        	'isEnabled' => true
        };
        String jsonMCCData = JSON.serialize(mccData);

        Test.startTest();

		ccrz.cc_RemoteActionResult result = cc_hello_ctrl_pmt_MCC_New.saveCCNumber(ctx, jsonMCCData);
		System.assert(!result.success);

		Test.stopTest();
	}

	@isTest
	static void testFailSaveMCC() {
		ccrz.cc_RemoteActionContext ctx = new ccrz.cc_RemoteActionContext();
		ccrz.cc_RemoteActionResult result = cc_hello_ctrl_pmt_MCC_New.saveCCNumber(ctx, 'notvalid');
		System.assert(!result.success);
	}
}
