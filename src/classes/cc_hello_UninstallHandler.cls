global with sharing class cc_hello_UninstallHandler implements UninstallHandler {

	global void onUninstall(UninstallContext ctx) {
		removeHelloWorldSubPage();
	}

	private void removeHelloWorldSubPage() {
		delete [select Id from ccrz__E_Subscriber_Page__c where ccrz__PageKey__c = 'ccrzhello'];
	}
}