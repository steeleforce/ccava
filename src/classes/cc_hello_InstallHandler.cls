global with sharing class cc_hello_InstallHandler implements InstallHandler {
	global void onInstall(InstallContext context) {
		if(context.previousVersion() == null) {
			setupHelloWorldSubPage();
			addPageConfigSettings();
			addPageLabels();
		}
		if(context.isUpgrade()) {
			setupHelloWorldSubPage();
			addPageConfigSettings();
			addPageLabels();
		}
	}

	// for use on dev orgs
	public static void runOnInstall() {
		cc_hello_InstallHandler handler = new cc_hello_InstallHandler();
		handler.setupHelloWorldSubPage();
		handler.addPageConfigSettings();
		handler.addPageLabels();
	}

	public void setupHelloWorldSubPage() {
		List<ccrz__E_Subscriber_Page__c> subPages = [select Id from ccrz__E_Subscriber_Page__c where ccrz__PageKey__c = 'ccrzhello'];
		if(subPages.isEmpty()) {
			ccrz__E_Subscriber_Page__c hello = new ccrz__E_Subscriber_Page__c(Name='HelloWorld', ccrz__PageKey__c='ccrzhello', ccrz__PageInclude__c='ccrzhello2__HelloWorld', ccrz__TitleLabel__c='Hello World', ccrz__Storefront__c='DefaultStore');
			insert hello;
		}
	}

	public void addPageConfigSettings() {
		ccrz.cc_util_Reflection.updatePageConfigurationEntry('Global.All.Pmt_MCC.New', 'ccrzhello2__cc_hello_pmt_MCC_New', true);
		ccrz.cc_util_Reflection.updatePageConfigurationEntry('Global.All.Pmt_MCC.Edit', 'ccrzhello2__cc_hello_pmt_MCC_Edit', true);
		ccrz.cc_util_Reflection.updatePageConfigurationEntry('Global.All.Pmt_MCC.Pay', 'ccrzhello2__cc_hello_pmt_MCC_Pay', true);
	}

	public void addPageLabels() {
		List<ccrz__E_PageLabel__c> pageLabels = new List<ccrz__E_PageLabel__c> {
			new ccrz__E_PageLabel__c(Name='MyWallet_AcctType_mcc', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='Mock Credit Card', ccrz__PageLabelId__c='MyWallet_AcctType_mccGlobalAll'),
			new ccrz__E_PageLabel__c(Name='MyWallet_AcctNum_mcc', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='{3} ending with {1}', ccrz__PageLabelId__c='MyWallet_AcctNum_mccGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentDisplay_mcc', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='{3} ending with {1}', ccrz__PageLabelId__c='PaymentDisplay_mccGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentProcessorTab_mcc', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='Mock Credit Card', ccrz__PageLabelId__c='PaymentProcessorTab_mccGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentType_Visa', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='VISA', ccrz__PageLabelId__c='PaymentType_VisaGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentType_MC', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='Mastercard', ccrz__PageLabelId__c='PaymentType_MCGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentType_Amex', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='AMEX', ccrz__PageLabelId__c='PaymentType_AmexGlobalAll'),
			new ccrz__E_PageLabel__c(Name='PaymentType_Disc', ccrz__PageName__c='All', ccrz__Storefront__c='Global', ccrz__Value__c='Discover', ccrz__PageLabelId__c='PaymentType_DiscGlobalAll')
		};
		upsert pageLabels ccrz__PageLabelId__c;
	}
}
